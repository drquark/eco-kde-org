---
title: Sustainable Software Goal
layout: sustainable-goal
#menu:
#   main:
#     name: sustainable-goal
#     weight: 3
---
Software has an impact on our future. It has an affect on energy and resource usage. KDE can deliver software which does this in a way which preserves environment and society for us and future generations. KDE can deliver Sustainable Software.

The sustainable software goal is about promoting sustainability in KDE by aligning existing activities, highlighting where our software is already sustainably designed, stimulating actions to increase sustainability, and creating standards/tools to quantify software sustainability. 